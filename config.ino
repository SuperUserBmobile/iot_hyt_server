static void prepareIndex(){
  s += "<!DOCTYPE html>";
  s += "<html>";
  s += "<head>";
  s += "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\" charset=\"ISO-8859-1\">";
  s += "</head>";
  s += "<style>";
  s += "html{";
  s += "font-family: Helvetica;";
  s += "display: inline-block;";
  s += "margin: 0px auto;";
  s += "text-align: center;";
  s += "}";
  s += ".form-control{";
  s += "width: 50%;";
  s += "background-color: white;";
  s += "}";
  s += "</style>";
  s += "<body>";
  s += "<h1>Configuracion Humedad y Temperatura</h1>";
  s += "<form action=\"/get\">";
  s += "<p>WiFi SSID</p>";
  s += "<input class=\"form-control\" id=\"ssid\" name=\"ssid\" type=\"text\" required>";
  s += "<p>WiFi Password</p>";
  s += "<input class=\"form-control\" id=\"password\" name=\"password\" type=\"text\" required>";
  s += "<p><div>";
  s += "<button class=\"button\" type=\"submit\">Enviar datos</button>";
  s += "</div></p>";
  s += "</form>";
  s += "</body>";
  s += "</html>";
}

/*
Description: Read Temperature and Humidity (Type 1)
Configuration: Using aREST(POST) and Serial
Connectivity: Wi-Fi and Ethernet
Platform: WebService
Database: AWS MySQL
Last update: 2019-05-13
Engineer: Erick Santiago


//==================== LIBRERIAS
#include <WiFi.h>
#include <ETH.h>
#include <EEPROM.h>
#include <Adafruit_SHT31.h>
#include <ArduinoJson.h>
#include <aREST.h>
#include "SSD1306Wire.h"

#define oled_sda    14
#define oled_scl    32
#define button_mode 34
#define led_R_T      5
#define led_G_T     16
#define led_B_T      4
#define led_R_H     33
#define led_G_H     15
#define led_B_H     13
#define sht_sda     14
#define sht_scl     32
#define led_wifi     5
#define ir_sensor   35
#define ir_enable   12
#define eth_clk_en  17

//==================== OBJETOS
SSD1306Wire OLED(0x3c, oled_sda, oled_scl); //Pantalla
Adafruit_SHT31 sht31 = Adafruit_SHT31();
aREST rest = aREST();

//==================== WS CONFIG
String wifi_ssid;
String wifi_pass;
String local_ip;
String gateway;
String subnet;
String dns1;
String dns2;
String ws_server;
String ws_directory;
String ws_config;
String ws_database;
String ws_payment = "/validapago";            //ELIMINAR SI ES PARA VENTA
String ws_port = "80";
WiFiClient ws_client;
//WiFiServer server(ws_port.toInt());
WiFiServer server(80);

//==================== SENSOR
int device_type = 1;
char platform[] = "AWS";
int id_device;
String device_name;
String url_umbral;
String bt_name;
String mac_address;
String aws_policy;
String host_address;
String topic_name;

//==================== STATIC IP
int li_ip_1,li_ip_2,li_ip_3,li_ip_4;
int *ptr_li_ip_1 = &li_ip_1;
int *ptr_li_ip_2 = &li_ip_2;
int *ptr_li_ip_3 = &li_ip_3;
int *ptr_li_ip_4 = &li_ip_4;
int gw_ip_1,gw_ip_2,gw_ip_3,gw_ip_4;
int *ptr_gw_ip_1 = &gw_ip_1;
int *ptr_gw_ip_2 = &gw_ip_2;
int *ptr_gw_ip_3 = &gw_ip_3;
int *ptr_gw_ip_4 = &gw_ip_4;
int sn_ip_1,sn_ip_2,sn_ip_3,sn_ip_4;
int *ptr_sn_ip_1 = &sn_ip_1;
int *ptr_sn_ip_2 = &sn_ip_2;
int *ptr_sn_ip_3 = &sn_ip_3;
int *ptr_sn_ip_4 = &sn_ip_4;
int dns1_ip_1,dns1_ip_2,dns1_ip_3,dns1_ip_4;
int *ptr_dns1_ip_1 = &dns1_ip_1;
int *ptr_dns1_ip_2 = &dns1_ip_2;
int *ptr_dns1_ip_3 = &dns1_ip_3;
int *ptr_dns1_ip_4 = &dns1_ip_4;
int dns2_ip_1,dns2_ip_2,dns2_ip_3,dns2_ip_4;
int *ptr_dns2_ip_1 = &dns2_ip_1;
int *ptr_dns2_ip_2 = &dns2_ip_2;
int *ptr_dns2_ip_3 = &dns2_ip_3;
int *ptr_dns2_ip_4 = &dns2_ip_4;


IPAddress local_ip_add(li_ip_1,li_ip_2,li_ip_3,li_ip_4);
IPAddress gateway_add(gw_ip_1,gw_ip_2,gw_ip_3,gw_ip_4);
IPAddress subnet_add(sn_ip_1,sn_ip_2,sn_ip_3,sn_ip_4);
IPAddress dns1_add(dns1_ip_1,dns1_ip_2,dns1_ip_3,dns1_ip_4);
IPAddress dns2_add(dns2_ip_1,dns2_ip_2,dns2_ip_3,dns2_ip_4);

IPAddress local_ip_add(172, 16, 9, 133); 
IPAddress gateway_add(172, 16, 8, 1);
IPAddress subnet_add(255, 255, 252, 0);
IPAddress dns1_add(172, 16, 2, 170);
IPAddress dns2_add(172, 16, 2, 100);


//=============== TIEMPOS
int interval = 300000;
int min_alerts = 5;
int msgCount = 0;
int msgReceived = 0;
int first_msg;
int fail_count;
int timeout;
int checks_eeprom;
static uint64_t previous_millis;
static uint64_t previous_oled;

//=============== EEPROM (512 bytes)
int mode_dir = 0;           // en   0       -   1 Byte
int ssid_dir = 2;           // de   2 a  33 -  32 Bytes
int pass_dir = 35;          // de  35 a  66 -  32 Bytes
int local_ip_dir = 68;      // de  68 a  82 -  15 Bytes
int gateway_dir = 84;       // de  84 a  98 -  15 Bytes
int subnet_dir = 100;       // de 100 a 114 -  15 Bytes
int dns1_dir = 116;         // de 116 a 130 -  15 Bytes
int dns2_dir = 132;         // de 132 a 146 -  15 Bytes
int ws_server_dir = 148;    // de 148 a 211 -  64 Bytes
int ws_port_dir = 213;      // de 213 a 217 -   5 bytes
int ws_directory_dir = 219; // de 219 a 346 - 128 Bytes
int ws_config_dir = 348;    // de 348 a 411 -  64 bytes
int ws_database_dir = 413;  // de 413 a 476 -  64 bytes

//=============== VARIABLES
int alerts;
int alert_count;
int alert_moved = 0;
int temp_high;
int temp_low;
int hum_high;
int hum_low;
float temperature;
float humidity;
float probability;
float cny;
float mlt_prom;
float mlt_total;
float mlh_prom;
float mlh_total;
float count_mlt;
float count_mlh;
float prob_temp;
float prob_hum;
boolean payment;
boolean request_ok;

//=============== STATUS
int error_status;
String device_mode;
String old_device_mode;
bool has_wifi = false;
bool has_eth = false;
bool status_ble = false;
bool device_connected = false;
bool messageSend = false;
bool config_ok = false;
bool wifi_data_status = false;
bool static_ip_status = false;
bool webservice_status = false;

static void resetDevice()
{
  Serial.println("Reseting Device...");
  
  device_mode = resetMODE();
  EEPROM.writeString(mode_dir, device_mode); EEPROM.commit();

  wifi_ssid = reset(32);
  EEPROM.writeString(ssid_dir, wifi_ssid); EEPROM.commit();
  
  wifi_pass = reset(32);
  EEPROM.writeString(pass_dir, wifi_pass); EEPROM.commit();

  local_ip = reset(15);
  EEPROM.writeString(local_ip_dir, local_ip); EEPROM.commit();

  gateway = reset(15);
  EEPROM.writeString(gateway_dir, gateway); EEPROM.commit();

  subnet = reset(15);
  EEPROM.writeString(subnet_dir, subnet); EEPROM.commit();

  dns1 = reset(15);
  EEPROM.writeString(dns1_dir, dns1); EEPROM.commit();

  dns2 = reset(15);
  EEPROM.writeString(dns2_dir, dns2); EEPROM.commit();

  ws_port = reset(5);
  EEPROM.writeString(ws_port_dir, ws_port); EEPROM.commit();

  ws_server = reset(64);
  EEPROM.writeString(ws_server_dir, ws_server); EEPROM.commit();

  ws_directory = reset(128);
  EEPROM.writeString(ws_directory_dir, ws_directory); EEPROM.commit();

  ws_config = reset(64);
  EEPROM.writeString(ws_config_dir, ws_config); EEPROM.commit();

  ws_database = reset(64);
  EEPROM.writeString(ws_database_dir, ws_database); EEPROM.commit();
} 

static void initData()
{
  WiFi.begin(); mac_address = WiFi.macAddress(); WiFi.disconnect();
  
  Serial.println("Obteniendo datos de EEPROM");
  device_mode = EEPROM.readString(mode_dir);
  Serial.print("MODE readed: "); Serial.print(device_mode); Serial.println(".");
  
  wifi_ssid = EEPROM.readString(ssid_dir);
  Serial.print("SSID readed: "); Serial.print(wifi_ssid); Serial.println(".");
  
  wifi_pass = EEPROM.readString(pass_dir);
  Serial.print("PASS readed: "); Serial.print(wifi_pass); Serial.println(".");

  local_ip = EEPROM.readString(local_ip_dir);
  Serial.print("STATIC IP readed: "); Serial.print(local_ip); Serial.println(".");
  ip_splitter(local_ip,ptr_li_ip_1, ptr_li_ip_2, ptr_li_ip_3, ptr_li_ip_4);
  Serial.print("STATIC IP Separado: "); Serial.print(li_ip_1); Serial.print(".");Serial.print(li_ip_2); Serial.print(".");Serial.print(li_ip_3); Serial.print(".");Serial.println(li_ip_4);
  
  gateway = EEPROM.readString(gateway_dir);
  Serial.print("GATEWAY readed: "); Serial.print(gateway); Serial.println(".");
  ip_splitter(gateway,ptr_gw_ip_1, ptr_gw_ip_2, ptr_gw_ip_3, ptr_gw_ip_4);
  Serial.print("GATEWAY Separado: "); Serial.print(gw_ip_1); Serial.print(".");Serial.print(gw_ip_2); Serial.print(".");Serial.print(gw_ip_3); Serial.print(".");Serial.println(gw_ip_4);
  
  subnet = EEPROM.readString(subnet_dir);
  Serial.print("SUBNET readed: "); Serial.print(subnet); Serial.println(".");
  ip_splitter(subnet,ptr_sn_ip_1, ptr_sn_ip_2, ptr_sn_ip_3, ptr_sn_ip_4);
  Serial.print("SUBNET Separado: "); Serial.print(sn_ip_1); Serial.print(".");Serial.print(sn_ip_2); Serial.print(".");Serial.print(sn_ip_3); Serial.print(".");Serial.println(sn_ip_4);
  
  dns1 = EEPROM.readString(dns1_dir);
  Serial.print("DNS1 readed: "); Serial.print(dns1); Serial.println(".");
  ip_splitter(dns1,ptr_dns1_ip_1, ptr_dns1_ip_2, ptr_dns1_ip_3, ptr_dns1_ip_4);
  Serial.print("DNS1 Separado: "); Serial.print(dns1_ip_1); Serial.print(".");Serial.print(dns1_ip_2); Serial.print(".");Serial.print(dns1_ip_3); Serial.print(".");Serial.println(dns1_ip_4);
  
  dns2 = EEPROM.readString(dns2_dir);
  Serial.print("DNS2 readed: "); Serial.print(dns2); Serial.println(".");
  ip_splitter(dns2,ptr_dns2_ip_1, ptr_dns2_ip_2, ptr_dns2_ip_3, ptr_dns2_ip_4);
  Serial.print("DNS2 Separado: "); Serial.print(dns2_ip_1); Serial.print(".");Serial.print(dns2_ip_2); Serial.print(".");Serial.print(dns2_ip_3); Serial.print(".");Serial.println(dns2_ip_4);
  
  ws_port = EEPROM.readString(ws_port_dir);
  Serial.print("PORT readed: "); Serial.print(ws_port); Serial.println(".");

  ws_server = EEPROM.readString(ws_server_dir);
  if (ws_server == "") { ws_server = "desarrollo.scanda.com.mx"; } 
  Serial.print("SERVER readed: "); Serial.print(ws_server); Serial.println(".");
  
  ws_directory = EEPROM.readString(ws_directory_dir);
  if (ws_directory == "") { ws_directory = "/serviciosBmobileQA/sensor_aws_api/dashboard/sensor"; } 
  Serial.print("DIRECTORY readed: "); Serial.print(ws_directory); Serial.println(".");

  ws_config = EEPROM.readString(ws_config_dir);
  if (ws_config == "") { ws_config = "/obtenerconfiguracion"; } 
  Serial.print("CONFIG readed: "); Serial.print(ws_config); Serial.println(".");

  ws_database = EEPROM.readString(ws_database_dir);
  if (ws_database == "") { ws_database = "/insertaDatosSensor"; } 
  Serial.print("DATABASE readed: "); Serial.print(ws_database); Serial.println(".");
}

static void saveData()
{
  EEPROM.writeString(mode_dir, device_mode); EEPROM.commit();
  EEPROM.writeString(ssid_dir, wifi_ssid); EEPROM.commit();
  EEPROM.writeString(pass_dir, wifi_pass); EEPROM.commit();
  EEPROM.writeString(local_ip_dir, local_ip); EEPROM.commit();
  EEPROM.writeString(gateway_dir, gateway); EEPROM.commit();
  EEPROM.writeString(subnet_dir, subnet); EEPROM.commit();
  EEPROM.writeString(dns1_dir, dns1); EEPROM.commit();
  EEPROM.writeString(dns2_dir, dns2); EEPROM.commit();
  EEPROM.writeString(ws_port_dir, ws_port); EEPROM.commit();
  EEPROM.writeString(ws_server_dir, ws_server); EEPROM.commit();
  EEPROM.writeString(ws_directory_dir, ws_directory); EEPROM.commit();
  EEPROM.writeString(ws_config_dir, ws_config); EEPROM.commit();
  EEPROM.writeString(ws_database_dir, ws_database); EEPROM.commit();
}

static void changeModeSC()
{
  if (digitalRead(button_mode) == HIGH)
  {
    device_mode = "S";
  }
  else if (digitalRead(button_mode) == LOW)
  {
    device_mode = "W";
  }
}

static void changeModeWF()
{
  if (digitalRead(button_mode) == HIGH)
  {
    device_mode = "W";
  }
  else if (digitalRead(button_mode) == LOW)
  {
    device_mode = "S";
  }
}

void WiFiEvent(WiFiEvent_t event)
{
  switch(event)
  {
    //==================== WIFI
    case SYSTEM_EVENT_STA_START:
      Serial.println("WiFi Started");
      delay(250);
      break;
    case SYSTEM_EVENT_STA_CONNECTED:
      Serial.println("WiFi Connected");
      if(static_ip_status){
        IPAddress local_ip_add(li_ip_1,li_ip_2,li_ip_3,li_ip_4);
        IPAddress gateway_add(gw_ip_1,gw_ip_2,gw_ip_3,gw_ip_4);
        IPAddress subnet_add(sn_ip_1,sn_ip_2,sn_ip_3,sn_ip_4);
        IPAddress dns1_add(dns1_ip_1,dns1_ip_2,dns1_ip_3,dns1_ip_4);
        IPAddress dns2_add(dns2_ip_1,dns2_ip_2,dns2_ip_3,dns2_ip_4);
        Serial.println("Conectando a IP Estatica WiFi");
        WiFi.config(local_ip_add,gateway_add,subnet_add,dns1_add,dns2_add);
      }
      delay(250);
      break;
    case SYSTEM_EVENT_STA_GOT_IP:
      Serial.print("WiFi MAC: "); Serial.println(WiFi.macAddress());
      Serial.print("IPv4 WiFi: "); Serial.println(WiFi.localIP());
      has_wifi = true;
      break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
      Serial.println("WiFi Disconnected");
      delay(250);
      has_wifi = false;
      break;
    case SYSTEM_EVENT_STA_STOP:
      Serial.println("WiFi Stopped");
      has_wifi = false;
      break;
    //==================== ETHERNET
    case SYSTEM_EVENT_ETH_START:
      Serial.println("ETH Started");
      delay(250);
      break;
    case SYSTEM_EVENT_ETH_CONNECTED:
      Serial.println("ETH Connected");
      if(static_ip_status){
        IPAddress local_ip_add(li_ip_1,li_ip_2,li_ip_3,li_ip_4);
        IPAddress gateway_add(gw_ip_1,gw_ip_2,gw_ip_3,gw_ip_4);
        IPAddress subnet_add(sn_ip_1,sn_ip_2,sn_ip_3,sn_ip_4);
        IPAddress dns1_add(dns1_ip_1,dns1_ip_2,dns1_ip_3,dns1_ip_4);
        IPAddress dns2_add(dns2_ip_1,dns2_ip_2,dns2_ip_3,dns2_ip_4);
        Serial.println("Conectando a IP Estatica Ethernet");
        ETH.config(local_ip_add,gateway_add,subnet_add,dns1_add,dns2_add);
      }
      WiFi.disconnect();
      delay(250);
      break;
    case SYSTEM_EVENT_ETH_GOT_IP:
      Serial.print("ETH MAC: "); Serial.println(ETH.macAddress());
      Serial.print("IPv4 ETH: "); Serial.println(ETH.localIP());
      has_eth = true;
      break;
    case SYSTEM_EVENT_ETH_DISCONNECTED:
      Serial.println("Eth Disconnected");
      WiFi.begin(wifi_ssid.c_str(), wifi_pass.c_str());
      delay(250);
      has_eth = false;
      break;
    case SYSTEM_EVENT_ETH_STOP:
      Serial.println("Eth Stopped");
      has_eth = false;
      break;
    //====================
    default:
      break;  
  }
}

void serialConfig()
{
  char input_char_01 = Serial.read();
  String str_rcvd;
  switch(input_char_01)
  {
    case 'a':
      while(Serial.available())
      {
        input_char_01 = Serial.read();
        str_rcvd.concat(input_char_01);
      }
      wifi_ssid = str_rcvd;
      EEPROM.writeString(ssid_dir, wifi_ssid); EEPROM.commit();
      wifi_ssid = EEPROM.readString(ssid_dir);
      OLED.clear();
      OLEDCentro("SSID");
      break;
    case 'b':
      while(Serial.available())
      {
        input_char_01 = Serial.read();
        str_rcvd.concat(input_char_01);
      }
      wifi_pass = str_rcvd;
      EEPROM.writeString(pass_dir, wifi_pass); EEPROM.commit();
      OLED.clear();
      OLEDCentro("Password");
      break;
    case 'c':
      while(Serial.available())
      {
        input_char_01 = Serial.read();
        str_rcvd.concat(input_char_01);
      }
      local_ip = str_rcvd;
      EEPROM.writeString(local_ip_dir, local_ip); EEPROM.commit();
      OLED.clear();
      OLEDCentro("Local IP");
      ip_splitter(local_ip,ptr_li_ip_1, ptr_li_ip_2, ptr_li_ip_3, ptr_li_ip_4);
      break;
    case 'd':
      while(Serial.available())
      {
        input_char_01 = Serial.read();
        str_rcvd.concat(input_char_01);
      }
      gateway = str_rcvd;
      EEPROM.writeString(gateway_dir, gateway); EEPROM.commit();
      OLED.clear();
      OLEDCentro("Gateway");
      ip_splitter(gateway,ptr_gw_ip_1, ptr_gw_ip_2, ptr_gw_ip_3, ptr_gw_ip_4);
      break;
    case 'e':
      while(Serial.available())
      {
        input_char_01 = Serial.read();
        str_rcvd.concat(input_char_01);
      }
      subnet = str_rcvd;
      EEPROM.writeString(subnet_dir, subnet); EEPROM.commit();
      OLED.clear();
      OLEDCentro("SubNet");
      ip_splitter(subnet,ptr_sn_ip_1, ptr_sn_ip_2, ptr_sn_ip_3, ptr_sn_ip_4);
      break;
    case 'f':
      while(Serial.available())
      {
        input_char_01 = Serial.read();
        str_rcvd.concat(input_char_01);
      }
      dns1 = str_rcvd;
      EEPROM.writeString(dns1_dir, dns1); EEPROM.commit();
      OLED.clear();
      OLEDCentro("DNS 1");
      ip_splitter(dns1,ptr_dns1_ip_1, ptr_dns1_ip_2, ptr_dns1_ip_3, ptr_dns1_ip_4);
      break;
    case 'g':
      while(Serial.available())
      {
        input_char_01 = Serial.read();
        str_rcvd.concat(input_char_01);
      }
      dns2 = str_rcvd;
      EEPROM.writeString(dns2_dir, dns2); EEPROM.commit();
      OLED.clear();
      OLEDCentro("DNS 2");
      ip_splitter(dns2,ptr_dns2_ip_1, ptr_dns2_ip_2, ptr_dns2_ip_3, ptr_dns2_ip_4);
      break;
    case 'h':
      while(Serial.available())
      {
        input_char_01 = Serial.read();
        str_rcvd.concat(input_char_01);
      }
      ws_port = str_rcvd;
      EEPROM.writeString(ws_port_dir, ws_port); EEPROM.commit();
      OLED.clear();
      OLEDCentro("Port");
      break;
    case 'i': //ws_server
      while(Serial.available())
      {
        input_char_01 = Serial.read();
        str_rcvd.concat(input_char_01);
      }
      ws_server = str_rcvd;
      EEPROM.writeString(ws_server_dir, ws_server); EEPROM.commit();
      OLED.clear();
      OLEDCentro("Server");
      break;
    case 'j': //ws_directory
      while(Serial.available())
      {
        input_char_01 = Serial.read();
        str_rcvd.concat(input_char_01);
      }
      ws_directory = str_rcvd;
      EEPROM.writeString(ws_directory_dir, ws_directory); EEPROM.commit();
      OLED.clear();
      OLEDCentro("Directory");
      break;
    case 'k': //Nombre del directorio de configuración
      while(Serial.available())
      {
        input_char_01 = Serial.read();
        str_rcvd.concat(input_char_01);
      }
      ws_config = str_rcvd;
      EEPROM.writeString(ws_config_dir, ws_config); EEPROM.commit();
      OLED.clear();
      OLEDCentro("Config");
      break;
    case 'l': //Nombre del directorio de base de datos
      while(Serial.available())
      {
        input_char_01 = Serial.read();
        str_rcvd.concat(input_char_01);
      }
      ws_database = str_rcvd;
      EEPROM.writeString(ws_database_dir, ws_database); EEPROM.commit();
      OLED.clear();
      OLEDCentro("Database");
      break;
    case 'x':
      OLED.clear();
      OLEDCentro("RESET");
      resetDevice();
      delay(3000);
      ESP.restart();
      break;
    case '0':
      OLED.clear();
      OLEDCentro("Config. OK");
      device_mode = "W";
      EEPROM.writeString(mode_dir, device_mode); EEPROM.commit();
      device_mode = EEPROM.readString(mode_dir);
      OLED.clear();
      OLEDCentro(device_mode);
      delay(3000);
      config_ok = true;
      break;
  }
}
  
void setup()
{
  Serial.begin(9600);
  while (!Serial) {;}
  pinMode(ir_enable, OUTPUT);
  pinMode(ir_sensor, INPUT);
  pinMode(eth_clk_en, OUTPUT);
  pinMode(button_mode, INPUT);
  digitalWrite (ir_enable, HIGH);
  EEPROM.begin(512);
  initRGB();
  InitOLED();
  OLED.clear();
  OLEDCentro("Starting");
  delay(5000);
  device_mode = EEPROM.readString(mode_dir);
  checks_eeprom = 0;
  while((device_mode != "S") && (device_mode != "W"))
  {
    device_mode = EEPROM.readString(mode_dir);
    Serial.print("Dispositivo en modo: ");
    Serial.println(device_mode);
    delay(500);
    checks_eeprom++;
    if (checks_eeprom == 10)
      resetDevice();
  }
  initData();
  if (device_mode == "S")
  {
    OLEDCentro2("USB","Serial");
    blue();
    while(!config_ok)
    {
      if (Serial.available() > 0)
      {
        serialConfig();
      }
      changeModeWF();
      if(device_mode == "W")
      {
        OLED.clear();
        OLEDCentro("Restart");
        EEPROM.writeString(mode_dir, device_mode); EEPROM.commit();  
        ESP.restart();
      }
    }
    OLED.clear();
    OLEDCentro("Saving");
    delay(3000);
    ESP.restart();
  }
  else if(device_mode == "W")
  {
    if (local_ip == "")
    {
      static_ip_status = false;
    }
    else
    {
      static_ip_status = true;
    }
    white();
    OLEDCentro2("Active","Mode");
    digitalWrite (eth_clk_en, HIGH);
    WiFi.onEvent(WiFiEvent);
    ETH.begin();
    WiFi.begin(wifi_ssid.c_str(), wifi_pass.c_str());
    has_wifi = false;
    while(!has_wifi && !has_eth)
    {
      Serial.print(".");
      changeModeSC();
      if(device_mode == "S")
      {
        OLED.clear();
        OLEDCentro("Restart");
        EEPROM.writeString(mode_dir, device_mode); EEPROM.commit();  
        ESP.restart();
      }
      delay(1000);
    }
    Serial.println("Iniciando Server");
    OLED.clear();
    OLEDCentro("Wait WS");
    server.begin();
    request_ok = false;
    while (!server) {;}
    while(!request_ok)
    {
      WS_Config();
      delay(500);
      changeModeSC();
      if(device_mode == "S")
      {
        OLED.clear();
        OLEDCentro("Restart");
        EEPROM.writeString(mode_dir, device_mode); EEPROM.commit();  
        ESP.restart();
      }
    }
    request_ok = false;
    while(!request_ok)
    {
      WS_Thold();
      delay(500);
    }
    topic_name = device_type + topic_name;
  }
  sht31.begin();
  mlt_prom = sht31.readTemperature();
  mlh_prom = sht31.readHumidity();
  mlt_total = mlt_prom;
  mlh_total = mlh_prom;
  count_mlt = 1;
  count_mlh = 1;
  previous_millis = millis();
  previous_oled = millis();
  old_device_mode = device_mode;
  alerts = 0;
  alert_count = 0;
  alert_moved = 0;
  first_msg = 1;
  fail_count = 0;
  error_status = 0;
}

void loop() 
{
  int time_gap = (int)(millis()-previous_millis);
  int oled_gap = (int)(millis()-previous_oled);
  int previous_temp_high = temp_high;
  int previous_temp_low = temp_low;
  int previous_hum_high = hum_high;
  int previous_hum_low = hum_low;
  temperature = sht31.readTemperature(); //Lee Temperatura
  temperature -= 10.0;
  humidity = sht31.readHumidity(); //Lee Humedad
  humidity += 8.0;
  probability = getProbability();
  
  if (temperature >= temp_high || temperature <= temp_low || humidity >= hum_high || humidity <= hum_low)
    alert_count ++;
  else 
  {
    alert_count = 0;
    alerts = 0;
  }  
  if (prob_temp >= 100)
  {
    prob_temp = 100;
    redTemp();
    error_status = 711;
  }
  else if (prob_temp < 60)
  {
    greenTemp();
    count_mlt++;
    mlt_total = mlt_total + temperature;
    mlt_prom = mlt_total/count_mlt;
  }
  else
  {
    yellowTemp();
    error_status = 712;
  }
  if (prob_hum >= 100)
  {
    prob_hum = 100;
    redHum();
    error_status = 721;
  }
  else if (prob_hum < 60)
  {
    greenHum();
    count_mlh++;
    mlh_total = mlh_total + humidity;
    mlh_prom = mlh_total / count_mlh;
  }
  else
  {
    yellowHum();
    error_status = 722;
  } 
  if (has_wifi || has_eth)
  {
    if ((time_gap >= interval || alert_count == min_alerts || first_msg == 1 || alert_moved == 5) && payment)
    { 
      OLED.clear();
      OLEDCentro("Send Data"); 
      request_ok = false;
      while(!request_ok)
      {
        WS_Thold();
        delay(500);
      }
      request_ok = false;
      while(!request_ok)
      {
        WS_Pymt();
        delay(500);
      }
      if (alert_count >= 1)
      {
        alerts++;
      }
      String timestamp = getTime();
      int len = timestamp.length()+1;
      char fecha[len];
      timestamp.toCharArray(fecha, len);
      request_ok = false;
      while(!request_ok)
      {
        WS_Publish();
        delay(500);
      }
      first_msg = 0;     
      previous_millis = millis();
      if (previous_millis >= 2678400000)
      {
        OLEDCentro2("Month","Update");
        delay(3000);
        ESP.restart();
      }
    }
    if (oled_gap > 0 && oled_gap <= 1500)
      OLEDT(temperature);
    if (oled_gap > 1500 && oled_gap <= 3000)
      OLEDH(humidity);
    if (oled_gap > 3000 && oled_gap <= 4500)
      OLEDP(probability);
    if (oled_gap > 4500)
      previous_oled = millis();
  }
  else
  {
    OLEDCentro2("Network","Error");
  }
  changeModeSC();
  if(device_mode == "S")
  {
    OLED.clear();
    OLEDCentro("Restart");
    EEPROM.writeString(mode_dir, device_mode); EEPROM.commit();  
    ESP.restart();
  }
  if (analogRead(ir_sensor) >= 2048)
  {
    alert_moved ++;
    error_status = 731;
  }
  else
  {
    alert_moved = 0;
  }
  delay(500);
}
*/
