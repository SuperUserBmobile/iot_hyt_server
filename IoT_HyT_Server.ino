// Load Wi-Fi library
#include <WiFi.h>
WiFiServer server(80);
const char* ssid     = "ESP32-Prueba";
const char* password = "123456789";
String header;
String s;

void setup() {
  Serial.begin(9600);
  WiFi.softAP(ssid, password);
  IPAddress IP = WiFi.softAPIP();
  
  Serial.print("Setting AP (Access Point) "); Serial.print("IP address: "); Serial.println(IP);
  server.begin();
  prepareIndex();
}

void loop(){
  WiFiClient client = server.available();   // Listen for incoming clients
  
  if (client){ 
    Serial.println("nuevo cliente");
    boolean currentLineIsBlank = true;  
    while (client.connected()) { 
      if (client.available()) {  
        char c = client.read();
        header += c;
        char *token;
        if(c=='\n' && currentLineIsBlank) {   
          client.println("HTTP/1.1 200 OK");
          client.println("Content-Type: text/html");
          client.println("Connection: close");
          client.println();
          client.println(s);
          
          if(header.indexOf("GET /get") >= 0) {
            Serial.println("");
            Serial.println("OBTENIENDO DATOS");
            int init_header = header.indexOf("GET /get");
            int end_header = header.indexOf(" HTTP/1.1");
            String sub_header = header.substring(init_header, end_header);
            int first_param = sub_header.indexOf("?");
            int second_param = sub_header.indexOf("&");
            String ssid = sub_header.substring(first_param +6, second_param);
            String password = sub_header.substring(second_param + 10);
            Serial.print(ssid); Serial.println(".");
            Serial.print(password); Serial.println(".");
            Serial.println("");
          }
          
          break;
        }
        if (c=='\n'){
          currentLineIsBlank = true; 
        }      
        else if (c != '\r') {
          currentLineIsBlank = false;
        }
      }
    }
    header = ""; 
    client.stop();  
    Serial.println("cliente desconectado");
  }
}
